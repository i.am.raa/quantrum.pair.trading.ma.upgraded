# README #

Подробное описание доступно в статье: [Бэктестинг: улучшаем поиск для парного трейдинга](https://quantrum.me/1009-backtesting-uluchshaem-poisk-dlya-parnogo-trejdinga/).

Внизу блокнота добавлен алгоритм Quantopian.

## Database fields (PostgreSQL) ##

* **symbol**	character varying(6)
* **dt**	date
* **open**	bigint [0]
* **high**	bigint [0]
* **low**	bigint [0]
* **close**	bigint [0]
* **volume**	numeric(20,0) [0]
* **adj**	numeric(20,0) NULL

